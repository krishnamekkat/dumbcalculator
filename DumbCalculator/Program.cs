﻿using DumbCalcLibrary;
using System;

namespace DumbCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter first:");
            int x = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter second");
            int y = int.Parse(Console.ReadLine());

            var calc = new CalcLib();

            Console.WriteLine("Result : " + calc.Add(x, y));

            Console.ReadLine();
        }
    }
}
